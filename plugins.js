module.exports = {
	scripts: [
		// PLUGINS
		'static/js/vendor/jquery.js',
		'static/js/vendor/TweenMax.js',
		'static/js/vendor/slick.js',
		'static/js/vendor/scrollIt.js',
		'static/js/vendor/gsapScrollTo.js',
		// CUSTOM
		'static/js/custom/main.js'
	]
};