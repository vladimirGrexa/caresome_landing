var webJS = {
	common: {}
};

$.js = function (selector) {
    return $('[data-js="' + selector + '"]');
};


//GLOBAL DECLARATION
var $w = $(window),
    $vW = $w.width(),
    $body = $('body'),
    $html = $('html');

webJS.common = (function (window, document) {

    function menuTrigger() {
        var headerMenu = $.js('header-menu'),
            headerMenuList = $.js('header-menu-list').find('li'),
            tween = new TimelineMax();

        $.js('mobile-menu-trigger').on("click", function () {
            headerMenu.addClass('is-active');
            tween.staggerFromTo(headerMenuList, 0.35, {
                x: -10,
                autoAlpha: 0
            }, {
                delay: 0.4,
                x: 0,
                autoAlpha: 1,
                ease: Power1.easeOut
            }, 0.2);
            $html.addClass('lock');
            $body.addClass('lock');
        });
        $.js('mobile-menu-close').on("click", function () {
            headerMenu.removeClass('is-active');
            $html.removeClass('lock');
            $body.removeClass('lock');
        });
        headerMenuList.on('click', function () {
            headerMenu.removeClass('is-active');
            $html.removeClass('lock');
            $body.removeClass('lock');
        });
    }

    function mainCarousel(){
        if ($.js('carousel').length) {
            var mainCarousel = $.js('carousel');
            mainCarousel.slick({
                infinite: true,
                arrows: false,
                dots: true,
                autoplay: false,
                speed: 800,
                slidesToShow: 1,
                fade: true,
                slidesToScroll: 1
            });
        }
    }

    function mouseMoveEffects() {
        $body.mousemove(function (e) {
            var x = e.pageX / window.innerWidth,
                y = e.pageY / window.innerHeight,
                moveTarget = $.js('move');
            x = x * -10;
            y = y * -10;

            TweenMax.to(moveTarget, 0.3, {
                x: x,
                y: y,
                ease: Power0.easeNone
            });
        });
    }

    function scrollNav() {
        $.scrollIt();
    }

    function smoothScroll() {
        var scrollTrigger = $.js('smooth-scroll');
        scrollTrigger.click(function () {
            var $this = $(this),
                href = $this.attr("href"),
                topY = $(href).offset().top,
                offsetY = 70;


            TweenLite.to($w, 0.5, {scrollTo: {y: topY - offsetY}, ease: Power2.easeOut});
            return false;
        });
    }



    return {
        menuTrigger: menuTrigger,
        mainCarousel: mainCarousel,
        mouseMoveEffects: mouseMoveEffects,
        scrollNav: scrollNav,
        smoothScroll: smoothScroll

	};

})(window, document);

$(function () {
	webJS.common.menuTrigger();
	webJS.common.mainCarousel();
	webJS.common.mouseMoveEffects();
	webJS.common.scrollNav();
	webJS.common.smoothScroll();
});

